import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require('path')
/**
 * https://vitejs.dev/config/
 * @type {import('vite').UserConfig}
 */
// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  server: {
    open: true,
    port: 3000,
  },
  plugins: [
    vue(),
  ],
  resolve: {
    // 别名
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  build: {
    cssCodeSplit: true, // 启用/禁用 CSS 代码拆分 禁用后 css拆成独立文件
    sourcemap: true, // 是否打包sourcemap
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['vue'],
      output: {
        assetFileNames: `vue3SeamlessScroll.[ext]`
      }
    },
    lib: {
      // 入口
      entry: 'src/components/vue3SeamlessScroll/index.js',
      // 暴露的全局变量
      name: 'vue3SeamlessScroll',
      // 输出的包文件名，默认 fileName 是 package.json 的 name 选项
      fileName: (format) => `vue3SeamlessScroll-${format}.js`,
    },
    // 指定输出路径（相对于 项目根目录).
    outDir: 'lib',
  },
})
