import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App)
// 全局安装
import vueSeamlessScroll  from '../lib/vue3SeamlessScroll-es.js'
app.use(vueSeamlessScroll)

app.mount('#app')
