/**
 * @description vue3SeamlessScroll滚动插件
 * @author Zhaohui Zeng
 * @date 2021-08-27 15:47:10
 */
import vue3 from './vue3.vue'
import vue2 from './vue2.vue'
export default {
    install: (app) => {
        // console.log(app)
        if(parseFloat(app.version) > 3) {
            // console.log('app.version： vue3')
            app.component(vue3.name, vue3)
        } else {
            app.component(vue2.name, vue2)
            // console.log('app.version： vue2')
        }
    }
}
