# vue3-seamless-scroll
> 一个兼容``vue 2.x-3.x`` 的``vue-seamless-scroll``区域滚动插件

[gitee](https://gitee.com/zengxiaohui2019/vue3-seamless-scroll)

[comment]: <> ([github]&#40;https://github.com/zengxiaohui2019/vue3-seamless-scroll&#41;)

## 安装
````shell
npm install @zengxiaohui/vue3-seamless-scroll --save
````
## 使用
### 全局安装
````javascript
// main.js
import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App)
import vueSeamlessScroll from '@zengxiaohui/vue3-seamless-scroll'
app.use(vueSeamlessScroll)

app.mount('#app')
````
### 局部应用
[demo.vue](https://gitee.com/zengxiaohui2019/vue3-seamless-scroll#/zengxiaohui2019/vue3-seamless-scroll/blob/master/src/components/demo.vue)
````html
<template>
    <div class="demo demo1">
        <vue-seamless-scroll :class-option="classOption" :data="list" @onChange="onChange">
            <p v-for="(item,index) in list" :key="index" :data-item="item+index">{{item}}</p>
        </vue-seamless-scroll>
    </div>
    <div class="demo demo2">
        <vue-seamless-scroll :class-option="classOption2" :data="list">
            <p v-for="(item,index) in list" :key="index">{{item}}</p>
        </vue-seamless-scroll>
    </div>
</template>

<script>
    import {ref} from "vue";
    import vueSeamlessScroll from '@zengxiaohui/vue3-seamless-scroll'
    export default {
        name: "demo",
        components: {vueSeamlessScroll}
        setup() {
            const list = ref([])
            for (let i = 0; i < 20; i++) {
                list.value.push('人生有很多出其不意的惊喜，比如你会以为我举个栗子')
            }
            const classOption = {
                singleHeight: 21+20, // 元素高度+下边距
                stop: false // 是否开启滚动
            }
            const classOption2 = {
                step: 0.5, // 速度
                hoverStop: true, // 鼠标停止
                direction: 1, // 0 下 1 上 2 左 3 右
            }
            // 点击事件回调 记得绑定:data-item="item+index" 可绑定多个属性
            function onChange(e) {
                console.log(e)
            }
            
            // 当前的滚动的index 用于单步滚动时的回调
            const index = ref(0)
            // 单次滚动结束 获取当前的index e是当前已经滚动的距离 务必配置singleHeight或singleWidth
            function SingScrollEnd(e){
                // console.log(e);
                let len = list.value.length
                index.value = (index.value + 1) % len
                console.log(index.value)
            }
            // 1秒后开始滚动 因为组件第一次滚动回调不会触发
            setTimeout(() => {
                classOption.autoPlay = true
                index.value++
            }, classOption.waitTime)
            
            
            return {
                classOption,
                classOption2,
                list,
                onChange,
                SingScrollEnd,
            }
        }
    }
</script>

<style scoped lang="scss">
    *{
        margin: 0;
        padding: 0;
    }
    .demo{
        width: 400px;
        height: 300px;
        border: 1px solid darkturquoise;
        overflow: hidden;
        p{
            margin-bottom: 20px;
        }
    }
    .demo2{
        margin-top: 50px;
    }
</style>
````

## 参数配置
在[vue-seamless-scroll](https://chenxuan0000.github.io/vue-seamless-scroll/zh/guide/) 基础上，增加以下属性：
````js
const option = {
    stop: false // 是否禁止滚动
}
````
> 如果想要配置超出容器才开始滚动 请结合 ``limitMoveNum``属性了来控制

## 点击事件回调 @onChange
````html
<vue-seamless-scroll :class-option="classOption" :data="list" @onChange="onChange">
    <p v-for="(item,index) in list" :key="index" :data-item="item+index">
        <span>{{item}}</span>
    </p>
</vue-seamless-scroll>
````
> 记得绑定 :data-item="item+index" 可绑定多个属性 如果是json请转换成json字符串进行绑定 eg: :data-item="JSON.stringify(item)"

> `组件<style scoped></style> ` 务必加上scoped 否则无法回调成功

## 单步滚动回调 @SingScrollEnd
````html
<vue-seamless-scroll :class-option="classOption" :data="list" @SingScrollEnd="SingScrollEnd">
    <p v-for="(item,index) in list" :key="index" :data-item="item+index">
        <span>{{item}}</span>
    </p>
</vue-seamless-scroll>
````
> @SingScrollEnd 只有开启单步滚动才会生效 配置singleHeight或singleWidth


## 更新日志

- 2021-09-02 v1.6.0 优化@onChange 点击回调事件获取不正确的bug
- 2021-09-06 v2.0.0 由于基于``seamless`` 会发生莫名其妙的滚动抖动bug[没找到原因],所以更换成``vue-seamless-scroll``,根据vue版本会自动加载不同版本组件
- 2021-09-06 v2.1.0 更新说明文档 去掉控制台日志
- 2021-09-06 v2.2.1 修复stop:true 鼠标移入移出后会滚动的bug
- 2021-09-06 v2.2.2 修复vue2.x 的stop:true 鼠标移入移出后会滚动的bug
- 2021-11-15 v2.2.3 增加单步滚动回调事件 ``@SingScrollEnd``
- 2021-12-22 v2.2.4 修复横向滚动 第二列未完全滚动出来时无法触发点击事件 `@onChange` 的bug
